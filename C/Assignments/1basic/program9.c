//WAP TO PRINT THE TABLE OF GIVEN NUMBER IN REVERSE ORDER
#include<stdio.h>
void main(){
	int n;
	printf("ENTER NUMBER OF WHICH YOU WANT TO PRINT THE TABLE OF :");
	scanf("%d",&n);
	for(int i=10;i>=1;i--){
		printf("%d x %d=%d\n",n,i,n*i);
	}
}
