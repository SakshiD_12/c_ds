//WAP TO PRINT THE EVEN NUMBERS IN REVERSE ORDER AND ODD NUMBERS IN STANDARD WAY BOTH SEPERATELY
#include<stdio.h>
void main(){
	int start,end;
	printf("ENTER STARTING RANGE :\n");
	scanf("%d",&start);		
	printf("ENTER ENDING RANGE :\n");
	scanf("%d",&end);
	
	printf("ODD NUMBERS IN STANDARD WAY :\n");
	for(int i=start;i<=end;i++){
		if(i%2==1){
		printf("%d\n",i);
		}
	}

	printf("EVEN NUMBERS IN REVERSE WAY :\n");
	for(int i=end;i>=start;i--){
		if(i%2==0){
		printf("%d\n",i);
		}
	}
}


