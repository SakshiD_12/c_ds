//WAP TO FUND THE SUM OF NUMBERS THAT ARE DIVISIBLE BY 5 IN GIVEN RANGE
#include<stdio.h>
void main(){
	int s,e;
	printf("ENTER STARTING RANGE :\n");
	scanf("%d",&s);
	printf("ENTER ENDING RANGE :\n");
	scanf("%d",&e);
	int sum=0;
	for(int i=s;i<=e;i++){
		if(i%5==0){
			printf("%d\n",i);
			sum=sum+i;
		}
	}
	printf("THE SUM OF NUMBER DIVISIBLE BY 5 IN GIVEN RANGE IS %d\n",sum);
}
