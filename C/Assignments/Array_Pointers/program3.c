
//WAP TO ADD TWO DIFFERENT ARRAYS OF THE SAME SIZE
#include<stdio.h>
void main(){

	int size;
	printf("ENTER SIZE OF ARRAY :\n");
	scanf("%d",&size);

	int arr1[size];
	int arr2[size];

	printf("ENTER ELEMENTS FOR ARRAY1 ;\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr1[i]);
	}
	
	printf("ENTER ELEMENTS FOR ARRAY2 ;\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr2[i]);
	}
	
	printf("THE ADDITION OF 2 ARRAYS IS :\n");
	for(int i=0;i<size;i++){

		printf("%d\n",arr1[i]+arr2[i]);
		
	}
	
	

}
