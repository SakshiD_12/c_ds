
//WAP TO SWAMP TWO NUMBERS 
//
#include<stdio.h>

void swap(int*,int*);

void main(){

	int x,y;
	printf("ENTER VALUE FOR X AND Y :\n");
	scanf("%d%d",&x,&y);
	printf("\n");

	swap(&x,&y);

}
void swap(int *ptr1,int *ptr2){

	printf("BEFORE SWAPPING :\n");
	printf("x=%d\n",*ptr1);
	printf("y=%d\n",*ptr2);
	printf("\n");

	int temp;
	temp=*ptr1;
	*ptr1=*ptr2;
	*ptr2=temp;
	
	printf("AFTER SWAPPING :\n");
	printf("x=%d\n",*ptr1);
	printf("y=%d\n",*ptr2);
}




