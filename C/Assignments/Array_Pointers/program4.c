
//WAP TO REVERSE THE ORDER OF ARRAY ELEMENTS 
#include<stdio.h>
void main(){

	int size;
	printf("ENTER SIZE OF ARRAY :\n");
	scanf("%d",&size);

	int arr[size];

	printf("ENTER ARRAY ELEMENTS ;\n");
	for(int i=0;i<size;i++){

		scanf("%d",&arr[i]);
	}
	
	int count1=0,count2=0;

	printf("ARRAY ELEMENTS ARE :\n");
	for(int i=0;i<size;i++){

		printf("%d\n",arr[i]);		
	}

	printf("ARRAY ELEMENTS IN REVERSE ORDER ARE :\n");
	for(int i=size-1;i>=0;i--){
		printf("%d\n",arr[i]);	
	}	
}
