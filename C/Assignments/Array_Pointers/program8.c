#include<stdio.h>
void main(){

	char arr[]={'A','B','C','D','E'};

	char *ptr=&arr[2];
	printf("%c\n",*ptr); //C

	(*ptr)++;
	printf("%c\n",*ptr); //D

	ptr=ptr+2;
	printf("%c\n",*ptr); //E
	
	printf("ARRAY ELEMENTS ARE :\n");
	for(int i=0;i<5;i++){
		printf("%c\n",arr[i]);
	}
}
