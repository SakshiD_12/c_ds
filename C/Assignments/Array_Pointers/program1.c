
//WAP TO FIND THE GIVEN ELEMENT FROM THE ARRAY
#include<stdio.h>
void main(){

	int size;
	printf("ENTER SIZE OF ARRAY :\n");
	scanf("%d",&size);

	int arr[size];

	printf("ENTER ARRAY ELEMENTS ;\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int search;
	printf("ENTER ELEMENT TO SEARCH :\n");
	scanf("%d",&search);

	int flag=0;
	printf("ARRAY ELEMENTS ARE :\n");
	for(int i=0;i<size;i++){
		printf("%d\n",arr[i]);
		if(arr[i]==search){
			flag=1;
		}
	}

	if(flag==1){
		printf("%d is present\n",search);
	}else{
		printf("%d is not present\n",search);
	}
}
