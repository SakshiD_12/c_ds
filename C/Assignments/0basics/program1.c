//write a program to print value ans size of the variable

#include<stdio.h>
void main(){
	int num=10;
	char ch='a';
	float rs=55.20;
	double crMoney=12345678.97654;

	printf("%d\n",num);
	printf("%c\n",ch);
	printf("%f\n",rs);
	printf("%lf\n",crMoney);

	printf("%ld\n",sizeof(int));
	printf("%ld\n",sizeof(char));
	printf("%ld\n",sizeof(float));
	printf("%ld\n",sizeof(double));
	printf("%ld\n",sizeof(void));

	}

