//4 TYPES OF STORAHE CLASS-----
//3.STATIC STORAGE CLASS---SCOPE IS BETWEEN THE FILE ONLY
//                      ---CONTAINS TWO TYPES OF VARIABLE---1.LOCAL STATIC VARIABLE 2.GLOBAL STATIC VARIABLE
//STATIC VARIABLE GET PLACED ON THE DATA SECTION                       
//STATIC LOCAL VARIABLE IS LIKE GLOBAL VARIABLE BUT BEHAVES LIKE LOCAL VARIABLE

//STATIC LOCAL VARIABLE 

#include<stdio.h>
void fun(){

	static int x=10;

	++x;
	printf("%d\n",x);
}
void main(){

	fun(); //11
	fun(); //12
	fun(); //13

}
