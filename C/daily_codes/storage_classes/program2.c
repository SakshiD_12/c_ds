//4 TYPES OF STORAGE CLASSES : 
//2.REGISTER---THE VARIABLE WHOSE STORAGE CLASS IS REGISTER GETS MEMORY ON CPU FOR VARIABLE
//          ---WE CANNOT GET THE ADDRESS OF VARIABLE FROM THE WHICH STORES ON CPU
//          ---IT WILL GIVE ERROR
#include<stdio.h>
void main(){

	register int x=10;

	printf("%d\n",x);
	printf("%p\n",&x); //error

}
