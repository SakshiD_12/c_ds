//4 TYPES OF STORAGE CLASSES---
//4.extern STORAGE CLASS----1)IF ANY OF VARIABLE IS WRITTEN USING THE extern STORAGE CLASS THEN IT IS ONLY THE 
//                            DECLARATION OF VARIABLE
//                          2)VARIABLE WRITTEN USING EXTERN WILL NOT GET PLACE ON RAM 
//                          3)HENCE IT WILL GIVE ERROR IF WE ACCESS THE EXTERN VARIABLE

#include<stdio.h>
void main(){

/*
        //NO ERROR
		
	int x;
	printf("%d\n",x); //GV
	printf("%p\n",&x); //address of x
*/
	//ERROR
	extern int y;
	printf("%d\n",y);
	printf("%p\n",&y);
}
