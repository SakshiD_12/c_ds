

//STORAGE CLASS ON FUNCTION :

//ONLY STATIC AND EXTERN STORAGE CLASSES CAN USE ON FUNCTION

//FUNCTION HAS EXTERN STORAGE CLASS BY DEFAULT 

#include<stdio.h>

extern void fun(){   //this line will execute successfully 

	printf("IN FUN\n");

}

void gun(){

	printf("IN GUN\n");

}

void main(){

	fun();
	gun();

}
