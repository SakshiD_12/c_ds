//ADDITION OF POINTER(CHARACTER POINTER)
//POINTER CAN ADD WITH CHARACTER ALSO AS CHARACTER IS INTERNALLY IS AN INTEGER
#include<stdio.h>
void main(){

	char ch1='A';
	char ch2='B';
	char ch3='C';

	char *ptr=&ch1;

	printf("ADDRESS OF CH1= %p\n",ptr); //address of ch1
	printf("%c\n",*ptr); //value at ptr i.e A

	printf("ADDRESS OF CH2= %p\n",ptr+1); // ADDRESS OF CH2
	printf("%c\n",*(ptr+1)); //B

	printf("ADDRESS OF CH3= %p\n",ptr+2); //ADDRESS OF CH3
	printf("%c\n",*(ptr+2)); //C

}
