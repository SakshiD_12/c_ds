//INTEGER VALUE CAN'T BE STORED DIRECTLY IN POINTER AS POINTER IS USED TO STORE ADDRESS ONLY....
#include<stdio.h>
void main(){

	int x=10;

	printf("%d\n",x); //10
	printf("%p\n",&x); //address of x

	int *ptr=x;;
	printf("%p\n",ptr); //warning (prints hexadecimal value of x i.e 10)
	printf("%p\n",&ptr); //address of pointer 
}	
