//POINTER TO AN ARRAY
#include<stdio.h>
void main(){
	
	int arr[5]={10,20,30,40,50};

	int *ptr1=arr; // STORES ADDRESS OF 1ST ELEMENT
	
	/*
	 warning
	int *ptr2=&arr; -------------STORE WHOLE ARRAY ADDRESS
	
	*/

	//solution
	int(*ptr2)[5]=&arr; //CAPABILITY TO STORE WHOLE ADDRESS

	printf("%p\n",ptr1); 
	printf("%p\n",ptr2);

	printf("%d\n",*ptr1); //10
	printf("%d\n",**ptr2); //10

}
