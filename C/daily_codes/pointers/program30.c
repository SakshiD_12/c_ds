//ARRAY OF POINTERS
#include<stdio.h>
void main(){

	int x=10;
	int y=20;
	int z=30;
	
	int arr[3]; //NORMAL ARRAY
	int* arr1[3]; //ARRAY OF POINTERS , STORES DATA IN 8 BYTE FOR EACH BLOCK

	printf("%ld\n",sizeof(arr)); //12
	printf("%ld\n",sizeof(arr1)); //24
}
