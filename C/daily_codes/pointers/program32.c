//ARRAY OF VOID POINTER 
#include<stdio.h>
void main(){

	int x=10;
	char ch='A';
	double d=20.50;

	void* arr[3]={&x,&ch,&d}; //void pointers can store the address of any data type

	printf("%p\n",arr[0]);
	printf("%p\n",arr[1]);
	printf("%p\n",arr[2]);

	printf("%d\n",*((int*)(arr[0])));
	printf("%c\n",*((char*)(arr[1])));
	printf("%lf\n",*((double*)(arr[2])));

}
