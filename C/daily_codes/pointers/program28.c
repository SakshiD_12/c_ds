//RELATIONSHIP BETWEEN 2D ARRAY AND POINTER 
#include<stdio.h>
void main(){
	
	int arr[3][3]={1,2,3,4,5,6,7,8,9};

	printf("%d\n",arr[0][2]); //3
	printf("%d\n",arr[1][2]); //6
	printf("%d\n",arr[2][2]); //9
	printf("%d\n",arr[0][1]); //2
	printf("%d\n",arr[1][1]); //5

	printf("%p\n",arr); //ADDRESS OF WHOLE ARRAY AS ADDRESS OF 1ST ELEMNT 
	printf("%p\n",arr[0]); //ADDRESS OF 1ST ELEMNT OF 0TH ROW
	printf("%p\n",arr[1]); //ADDRESS OF 1ST ELEMNT OF 1ST ROW
	printf("%p\n",arr[2]); //ADDRESS OF 1ST ELEMENT OF 2ND ROW

	printf("%p\n",&arr[0][1]); //address of 1st index elemnt on 0th row
	printf("%p\n",&arr[1][1]); //1st index on 1st row
	printf("%p\n",&arr[2][1]); //1st index of 2nd row
}

