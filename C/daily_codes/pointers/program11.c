//PRINTING ADDRESS OF ARRAY ELEMENT AT ANY INDEX USING POINTER
#include<stdio.h>
void main(){
	
	int arr[]={10,20,30,40};

	int *ptr1=&(arr[0]);
	int *ptr=&(arr[1]);

	printf("ADDRESS OF 0TH INDEX= %p\n",ptr1); //address of element stored at ptr1
	printf("ADDRESS OF 1ST INDEX= %p\n",ptr); //address of element stored
	printf("%d\n",*ptr1); //value at ptr1 i.e 10
	printf("%d\n",*ptr); //value at ptr i.e 20
}

