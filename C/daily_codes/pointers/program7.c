//PRINTING ADDRESS OF GLOBAL VARIABLE USING POINTER 
#include<stdio.h>

  	int x=10;
  	char ch='A';
	char ch1='B';
void main(){
	float f=12.5;
	double d=30.500;

	int *ptr1=&x;
	char *ptr2=&ch;
	float *ptr3=&f;
	double *ptr4=&d;
	char *ptr5= ch1; //warning

	printf(" ADDRESS OF PTR1(X)= %p\n",ptr1); //address of x
	printf(" ADDRESS OF PTR2(CH)= %p\n",ptr2); //address of ch
	printf("ADDRESS OF PTR3(F)= %p\n",ptr3); //address of f
	printf("ADDRESS OF PTR4(D)= %p\n",ptr4); //address of d

	printf("ADDRESS OF PTR5(CH2)= %p\n",ptr5); //hexadecimal value of B i.e 66(warning)

	printf("%d\n",*ptr1); //value at ptr1 i.e 10
	printf("%c\n",*ptr2); //value at ptr2 i.e A
	printf("%f\n",*ptr3); //value at ptr3 i.e 12.5
	printf("%lf\n",*ptr4); //value at ptr4 i.e 30.500
	
	//pointer can only used to store address but if we stored value it will print the value but at the time of accessing it will give
	//segmentation fault
	printf("%c\n",*ptr5);//segmentation fault 
	
	printf("END OF CODE\n");

}
