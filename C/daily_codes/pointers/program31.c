//ARRAY OF POINTERS 
//ARRAY CONTENT MEANS THE ADDRESS OF ELEMENTS
#include<stdio.h>
void main(){
	
	int x = 10;
	int y = 20;
	int z = 30;

	int* arr[3]={&x,&y,&z}; 

	printf("%p\n",arr[0]); //address of x
	printf("%p\n",arr[1]); //address of y
	printf("%p\n",arr[2]); //address of z

	printf("%d\n",*arr[0]); //10
	printf("%d\n",*arr[1]); //20
	printf("%d\n",*arr[2]); //30

	
}
