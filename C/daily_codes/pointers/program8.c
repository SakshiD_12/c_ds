//ADDITION OF POINTER (ADDITION OF TWO POINTER IS NOT POSSIBLE)
#include<stdio.h>
void main(){

	int x=10;
	int y=20;

	int *ptr1=&x;
	int *ptr2=&y;

	printf("%p\n",ptr1); //address of x
	printf("%p\n",ptr2); //address of y
	
	//POINTER ADDITION NOT ALLOWED BECAUSE IT STORES ADDRESS AND AFTER ADDITION OF ADDRESSES WE MAY GET ADDRESS OUT OF PROCESS RANGE 
	//  printf("%p\n",ptr1 + ptr2); //ERROR
	printf("%d\n",*ptr1 + *ptr2); //value at x + value at y=30
}
