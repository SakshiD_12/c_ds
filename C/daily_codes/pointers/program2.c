//PROGRAM TO PRINT THE ADDRESS OF POINTER
#include<stdio.h>
void main(){

	int x=10;
	printf("%d\n",x); //10
	printf("%p\n",&x); //hexadecimal address of x

	int *ptr=&x; 
	printf("%p\n",ptr); //stores the address of x
	printf("%p\n",&ptr); //adresss of pointer
}
