//DEREFERENCING VOID POINTER USING TYPE CASTING
#include<stdio.h>
void main(){

	int x=78;

	int *iptr=&x;
	void *vptr=&x;

	printf("%p\n",iptr); //address of x
	printf("%p\n",vptr); //address of x

	printf("%d\n",*iptr); //value at iptr(65)
	printf("%d\n",*((int*)vptr)); //65
	printf("%c\n",*((char*)vptr)); //A

}

