//SUBSTRACTION OF POINTER 
//SUBSTRACTION OF TWO POINTER IS POSSIBLE 
#include<stdio.h>
void main(){

	int arr[]={10,20,30,40,50};

	int *ptr1=&(arr[0]);
	int *ptr2=&(arr[3]);
	
	printf("%p\n",ptr1); //address of array element stored at ptr1 i.e address of 10
	printf("%p\n",ptr2); //address of array element stroed at ptr2 i.e address of 40

	printf("%d\n",*ptr1); //value at ptr1 i.e 10
	printf("%d\n",*ptr2); //value at ptr2 i.e 40

	//ptr1-ptr2 means
	//ptr1-ptr2 / size of data type of pointer 

	printf("%ld\n",ptr1-ptr2); //minus value i.e ptr1 is 3 block away from ptr2
	printf("%ld\n",ptr2-ptr1);
}
