//POINTER OF TYPE DOUBLE AND CHAR
#include<stdio.h>
void main(){

	double d=30.500;
	char ch='A';
	
	double *ptr=&d;
	char *ptr1=&ch;

	printf("%p\n",ptr); //address of d
	printf("%p\n",ptr1); //address of ch

	printf("%lf\n",*ptr); //value at *ptr i.e 30.500
	printf("%c\n",*ptr1); //value at *ptr1 i.e A

	printf("%d\n",*ptr1); //value at *ptr1 in ascii value of A i.e 65
}
