//POINTER TO AN ARRAY
#include<stdio.h>
void main(){
	
	int arr[5]={10,20,30,40,50};

	int *ptr1=arr; // STORES ADDRESS OF 1ST ELEMENT
	int *ptr2=&arr[2]; //30
	
	int(*ptr3)[5]=&arr; //CAPABILITY TO STORE WHOLE ADDRESS

	printf("%d\n",*ptr1); //10
	printf("%d\n",*ptr2); //30
	printf("%d\n",**ptr3); //10

	
	printf("%p\n",ptr1); //address of first element
	printf("%p\n",ptr3); //whole array address
	printf("%p\n",ptr2); //address of 30


}
