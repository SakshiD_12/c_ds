//DEREFERENCING OF VOID POINTER
#include<stdio.h>
void main(){

	int x=10;

	int *iptr=&x;
	void *vptr=&x;

	printf("%d\n",*iptr); //10
	printf("%d\n",*vptr); //ERROR

	//NOTE - DEREFERENCING OF VOID POINTER IS NOT ALLOWED 
	
}
