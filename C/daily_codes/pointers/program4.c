//DEREFERENCING OF POINTER (gives value stored at given address)  , * is used to dereferencing a pointer
#include<stdio.h>
void main(){

	int x=10;
	printf("%d\n",x); //10

	int *ptr=&x;
	printf("%p\n",ptr); //address of x
	printf("%d\n",*ptr); //value stored at x
}



