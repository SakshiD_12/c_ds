//VOID POINTER IS A GENERIC POINTER WHICH STORES ADDRESS OF ANY TYPE OF VARIABLE

#include<stdio.h>
void main(){

	int x=10;

	int *iptr=&x; //INTEGER POINTER
	void *vptr=&x; //VOID POINTER 

	printf("ADDRESS OF X USING INTEGER POINTER = %p\n",iptr); //ADDRESS OF X
	printf("ADDRESS OF X USING VOID POINTER = %p\n",vptr); //ADDRESS OF X

}


