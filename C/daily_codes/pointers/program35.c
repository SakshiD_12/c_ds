//POINTER TO POINTER 
#include<stdio.h>
void main(){

	int x=10;

	int *iptr1=&x; //iptr1 STORES THE ADDRESS OF X
	int *iptr2=iptr1; //iptr2 STORES THE VALUE STORES IN iptr1
	
	int **iptr3=&iptr1; //double pointer represent how far actual data is from iptr3
	int **iptr3=iptr1; //warning(double pointer is used to store addresss only not an data)

	printf("%d\n",x); //10
	printf("%d\n",*iptr1); //10
	printf("%d\n",*iptr2); //10
 	printf("%d\n",**iptr3); //10

}



