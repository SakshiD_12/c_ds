//CHANGING THE VALUE AT GIVEN ADDRESS
#include<stdio.h>
void main(){

	int x=10;
	printf("%d\n",x); //10

	int *ptr=&x;
	printf("%p\n",ptr); //address of x
	printf("%d\n",*ptr); //10

	//changing value at the address stored in pointer 
	
	*ptr=30;
	printf("%d\n",*ptr); //30

}

