#include<stdio.h>
void main(){

	int arr[]={10,20,30,40,50};

	int *ptr=&(arr[0]);
	printf("%d\n",*ptr); //10

	ptr++;
	printf("%d\n",*ptr); //20

	*ptr=70;
	printf("%d\n",*ptr); //70
	
	printf("FOR LOOP :\n");
	for(int i=0;i<5;i++){
		printf("%d\n",arr[i]); //10 70 30 40 50
	}
}
