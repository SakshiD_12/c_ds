//ADDITION OF POINTER WITH INTEGER (IT IS POSSIBLE TO ADD POINTER WITH INTEGER)
#include<stdio.h>
void main(){
	
	 //CONTIUOUS 4 BYTES OF MEMORU IS GIVEN TO X AND Y
	 int x=10;
	 int y=20;

	 int *ptr=&x;
	 //*(ptr+1) means
	 //value at(ptr + 1 *(size of data type of pointer(int=4))
	
	 printf("ADDRESS STORED AT PTR(X)= %p\n",ptr); //address of x

	 printf("AFTER ADDITION= %d\n",*(ptr+1));
}
