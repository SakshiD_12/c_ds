//DANGLING POINTER 

#include<stdio.h>
int a=10;
int b;
int *iptr=0;
void fun(){
	int x=30;
	printf("%d\n",a); //10
	printf("%d\n",b); //0

        iptr=&x;
	printf("%p\n",iptr); //address of x
	printf("%d\n",*iptr); //30
}
void main(){
	int y=40;
	printf("%d\n",a); //10
	printf("%d\n",b); //0

	fun();
	//HERE EVEN AFTER FUN() STACK FRAME IS POPED ALREADY IS SHOWS A ADDRESS OF X AND VALUE
	printf("%p\n",iptr); //address of x
	printf("%d\n",*iptr); //30
}





