#include<stdio.h>
void main(){

	int arr1[]={10,20,30,40,50};
	int arr2[]={60,70,80,90,100};

	int *iptr1 = arr1; //address of 1st element in arr1
	int *iptr2 = &arr1;//whole array address same as 1st element
	
	iptr1++;
	iptr2++;

	printf("%p\n",iptr1);
	printf("%d\n",*iptr1);//20

	printf("%p\n",iptr2);
	printf("%d\n",*iptr2);//20
}

