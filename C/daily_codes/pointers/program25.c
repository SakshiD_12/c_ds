//RELATION BETWEEN ARRAY AND POINTES (ARRAY IS INTERNALLY A POINTER)
#include<stdio.h>
void main(){
	int size;
	printf("ENTER SIZE OF ARRAY :\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENTER ELEMENTS :\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	for(int i=0;i<size;i++){
		//BELOW TWO LINES HAS SAME OUTPUT
		//printf("%d\n",arr[i]);
		printf("USING POINTER PRINTING THE ARRAY ELEMENTS =%d\n",*(arr+i));
	}
}

