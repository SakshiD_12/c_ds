//DEREFRENCING OF POINTER 
#include<stdio.h>
void main(){

	int x=10;
	
	int *ptr1=&x;
	int *ptr2=x;

	printf("%p\n",ptr1); //address of x
	printf("%p\n",ptr2); //hexadecimal value of 10
	
	printf("%d\n",*ptr1); //10
	printf("%d\n",*ptr2); //segmentation fault (code dumped) (as 10 is stored in ptr2 but 10 address is not present is process it is out of range)

	//whatever return after segmentation fULT WILL NOT DISPLAY IN THE OUTPUT
	
	printf("END OF CODE\n"); //this line will not show on the op screen 


}
