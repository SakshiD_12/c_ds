//PRINTING ADDRESS OF ALL ELEMNTS IN THE 2D ARRAY USING FOR LOOP
#include<stdio.h>
void main(){
	int r,c;
	printf("ENTER ROW AND COLUMN SIZE :\n");
	scanf("%d%d",&r,&c);
	
	int arr[r][c];
	printf("ENTER ARRAY ELEMENTS :\n");
	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++){
			scanf("%d",&arr[i][j]);
		}
	}

	printf("ARRAY ELEMNTS ARE:\n");
	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	printf("ADDRESESS OF ALL ELEMNTS IN 2D ARRAY :\n");
	for(int k=0;k<6;k++){
		printf("%p\n",arr[k]);
	}
}
	
