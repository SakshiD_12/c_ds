
//WAP TO PRINT THE SUM OF DIAGONAL ELEMENTS OF AN ARRAY
#include<stdio.h>
void main(){
	int r,c;
	printf("ENTER ROW AND COLUMN SIZE:\n");
	scanf("%d%d",&r,&c);

	int arr[r][c];
	printf("ENTER ARRAY ELEMENTS :\n");
	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++){
			scanf("%d",&arr[i][j]);
		}
	}

	int sum=0;

	printf("ARRAY ELEMENTS ARE :\n");
	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++){
			printf("%d ",arr[i][j]);
			if(arr[i]==arr[j]){
				sum=sum+arr[i][j];
			}
		}
		printf("\n");
	}
	printf("THE SUM OF DIAGONAL ELEMENTS IS :%d\n",sum);
}

