
//********************************************2D ARRAY*********************************************
//TAKING SIZE AND ARRAY ELEMENT ASINPUT FROM USER AND PRINTING THE ARRAY ELEMENTS 
#include<stdio.h>
void main(){
	int r,c;
	printf("ENTER ROW AND COLUMN SIZE:\n");
	scanf("%d%d",&r,&c);

	int arr[r][c];
	printf("ENTER ARRAY ELEMENTS :\n");
	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	printf("ARRAY ELEMENTS ARE :\n");
	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
}

