//PRINTIG 3D-ARRAY ELEMNTS BY TAKING SIZE FROM THE USER AND VALUES FROM THE USER
//add 0th row of 1st plane and 0th row of 2nd plane ans so on and print the sum

#include<stdio.h>
void main(){

	int p,r,c;
	printf("ENTER PLANE NUMBER :\n");
	scanf("%d",&p);

	printf("ENTER ROW NUMBER :\n");
	scanf("%d",&r);

	printf("ENTER COLUMN NUMBER :\n");
	scanf("%d",&c);

	int arr[p][r][c];

	printf("ENTER ARRAY ELEMENTS :\n");

	for(int i=0;i<p;i++){

		for(int j=0;j<r;j++){

			for(int k=0;k<c;k++){
				 
				scanf("%d",&arr[i][j][k]);
			}
		}
	}
	
	int sum=0;

	printf("ARRAY ELEMENTS ARE :\n");
		for(int i=0;i<p;i++){
			
			printf("PLANE=%d\n",i);

			for(int j=0;j<r;j++){

				for(int k=0;k<c;k++){

					printf("%d ",arr[i][j][k]);
					if(j==0){
						sum=sum+arr[i][j][k];
					}
				}
				printf("\n");
			}
		}
		printf("THE SUM IS :%d\n",sum);
}
