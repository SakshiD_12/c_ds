//WAP TO PRINT THE SUM OF ARRAY BY TAKING ARRAY ELEMNT AS INPUT FROM USER AND ALSO ARRAY SIZE AS INPUT
#include<stdio.h>
void main(){

	int size;
	printf("ENTER ARRAY SIZE :\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENTER ARRAY ELEMENTS :\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("ARRAY ELEMENTS ARE :\n");
	for(int i=0;i<size;i++){
		printf("%d\n",arr[i]);
	}
	
	int sum=0;
	for(int i=0;i<size;i++){
		sum=sum+arr[i];
	}
	printf("THE SUM OF ARRAY ELEMENT IS :%d\n",sum);
}

