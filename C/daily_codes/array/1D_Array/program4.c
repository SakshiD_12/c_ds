//PROGRAM TO PRINT THE ADDRESS OF ARRAY 
#include<stdio.h>
void main(){
	
	int arr[5]={10,20,30,40,50};
	int x=10;
	char ch='A';

	printf("%d\n",x);
	printf("%c\n",ch);
	
	printf("ADDRESS OF %d IS : %p\n ",x,&x);
//	printf("%p\n",&x);
	printf("ADDRESS OF %c IS : %p\n ",ch,&ch);
//	printf("%p\n",&ch);
	printf("ADDRESS OF ARRAY IS : %p\n ",arr);
//	printf("%p\n",arr);
}
