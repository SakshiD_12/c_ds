//STORING DATA USING ASSIGNING ELEMNTES ONE BY ONE METHOD 
#include<stdio.h>
void main(){

	char arr[5];

	 arr[0]='A';
	 arr[1]='B';
	 arr[2]='C';
	 arr[3]='D';
	 arr[4]='E';
	
	 printf("ARRAY ELEMENTS ARE :\n");
	 printf("%c\n",arr[0]);
	 printf("%c\n",arr[1]);
	 printf("%c\n",arr[2]);
	 printf("%c\n",arr[3]);
	 printf("%c\n",arr[4]);
	 printf("%c\n",arr[5]); //blank

	 printf("SIZE OF ARRAY IS : ");
	 printf("%ld\n",sizeof(arr)); //5
}

