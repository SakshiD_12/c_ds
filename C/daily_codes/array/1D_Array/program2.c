//PRINTING ARRAY ELEMENTS USING INDEX AND THE SIZE OF THE ARRAY
// STORING DATA USING INITIALIZER LIST 
#include<stdio.h>
void main(){

	int arr[]={10,20,30,40};

	printf("ARRAY ELEMENTS ARE :\n");
	printf("%d\n",arr[0]);
	printf("%d\n",arr[1]);
	printf("%d\n",arr[2]);
	printf("%d\n",arr[3]);
	printf("SIZE OF ARRAY IS : "); //4*4=16
	printf("%ld\n",sizeof(arr));

}

