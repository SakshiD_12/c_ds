
//2.realloc()---1)IF MEMORY ALLOCATED USING CALLOC IS LESS THE WE CAN ADD MORE MEMORY USING realloc()
//           ---2)realloc is used when we want to allocate memory dynamically
//           ---3)return type is void*
//           ---4)it has 2 parameters the one is variable jyachi memory free karychi ahe and second kiti cha change 
//               karychy
//           ---5)realloc() pahilya array chi sgli memory free krte 
//           ---6)realloc() la second parameter kadhich 0 dycha nasto
//                i.e (int*)realloc(ptr,0) is same as free()

#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr=(int*)calloc(5,sizeof(int)); //it will create array of size of of type int

	//TO INSERT VALUE IN ARRAY
	for(int i=0;i<5;i++){

		*(ptr+i)=10+i;

	}

	//TO PRINT THE VALUES OF ARRAY 
	for(int i=0;i<5;i++){

		printf("%d\n",*(ptr+i));

	}

	printf("PRINTING USING REALLOCATION CONCEPT \n");

	//REALLOCATION CONCEPT STARTS HERE
	//new pointer ptr2
	int *ptr2= (int*)realloc(ptr,8);  //it will change array size from 5 to 8 and free the first array of size 5

	for(int i=0;i<8;i++){

		*(ptr2+i)=20+i;

	}

	for(int i=0;i<8;i++){

		printf("%d\n",*(ptr+i));

	}

}
