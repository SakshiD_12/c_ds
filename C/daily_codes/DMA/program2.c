
//2.calloc()---1)stands for continuous memory location
//          ---2)it allocates memmory continuously and for multiple data
//          ---3)return type is void*
//          ---4)default value of calloc is 0
//          ---5)it has 2 parameters the one is how many data you want to allocate and second is what type of data 
//              you want to alloacte

#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr=(int*)calloc(5,sizeof(int)); //it will create array of size of of type int

	//TO INSERT VALUE IN ARRAY
	for(int i=0;i<5;i++){

		*(ptr+i)=10+i;

	}

	//TO PRINT THE VALUES OF ARRAY 
	for(int i=0;i<5;i++){

		printf("%d\n",*(ptr+i));

	}
}
