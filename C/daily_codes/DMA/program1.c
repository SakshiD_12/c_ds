
//DYNAMIC MEMORY ALLOCATION REQUIRES FOUR FUNCTIONS :
//  1.malloc() 
//  2.calloc()
//  3.realloc()
//  4. free()

//THIS ALL FUNCTION HELP US TO GET PLACE ON HEAP SECTION OF THE PROCESS

//1.malloc()---1)stands for memory allocation 
//          ---2)malloc la runtime la memory allocate hote
//          ---3)return type of malloc is void*
//          ---4)it is mandatory to use free() if we use malloc()
//          ---5)malloc heap section vr jaga milvun dete
//          ---6)malloc by default garbage value return krte
//          ---7)prototype of malloc :
//                         void* malloc(size_t ,variable_name)

#include<stdio.h>
#include<stdlib.h>

int mallocadd(){

	int *ptr1 = (int*)malloc(sizeof(int));
	
	int *ptr2 = (int*)malloc(sizeof(int));

	*ptr1=50;

	*ptr2=80;

	return *ptr1 + *ptr2;

}
void main(){

	int x=mallocadd();

	printf("%d\n",x);

}

