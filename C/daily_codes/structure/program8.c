
//OBJECT INITIALIZING USING MALLOC

#include<stdio.h>
#include<stdlib.h>

struct IPL{

	char sName[20];
	int tTeams;
	double price;

};

char* mystrcpy(char *dest,char *src){

	while(*src!='\0'){

		*dest=*src;
		dest++;
		src++;
	}
	*dest='\0';
	return dest;
}
void main(){

	//int *ptr=(int*)malloc(sizeof(int)); 
	
	struct IPL *obj=(struct IPL*)malloc(sizeof(struct IPL));

	mystrcpy((*obj).sName,"TATA");

	(*obj).tTeams = 8;

	//* and . is combinely returned as arrow ->
	
	obj->price=1000.50;

	//printing values 
	printf("%s\n",obj->sName);
	printf("%d\n",(*obj).tTeams);
	printf("%lf\n",obj->price);

}
