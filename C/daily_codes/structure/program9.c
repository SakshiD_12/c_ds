
//POINTER TO STRUCTURE

#include<stdio.h>

struct movie{

	char mName[20];
	int ntick;
	float rate;
};
void main(){

	struct movie obj={"TUMBAAD",2,4.9};

	//int *ptr=&x;
	
	struct movie *ptr =&obj;

	printf("%s\n",ptr->mName);
	printf("%d\n",(*ptr).ntick);
	printf("%f\n",ptr->rate);

}
