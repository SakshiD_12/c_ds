
//STRUCTURE STORES DATA OF DIFFERENT DATA TYPES

//CREATING OBECT OF STRUCTURE GLOBALLY SO IT WILL BE SEEN BY EVERYWHERE
#include<stdio.h>

struct cricplayer{

	char pName[20]; //BASE OF 4 AS IT IS GREATER AMONG ALL DATA TYPES HENCE CHAR ARRAY OF 20 REQIRES 20 BOX OF 1
	int jerNo;      //FOR EACH CHAR HENCE IT IS 20 + 4(INT) + 4(FLOAT)
	float avg;
}obj2={"MS.DHONI",7,45.75};  //************obect created globally****************
void main(){
	
	//assininh direct values to object
	struct cricplayer obj1={"VK",18,50.30};

	//printing the given value og object1
	printf("%s\n",obj1.pName);
	printf("%d\n",obj1.jerNo);
	printf("%f\n",obj1.avg);

	//PRINTING VALUES OF OBJECT 2
	printf("%s\n",obj2.pName);
	printf("%d\n",obj2.jerNo);
	printf("%f\n",obj2.avg);

	//size of structure
	printf("%ld\n",sizeof(obj2)); //28
	printf("%ld\n",sizeof(struct cricplayer)); //28

}

	

