

//INITIALIZING OBJECT(2WAYS)
//--------------------------> 1)OBJECT INITIALIZATION
//--------------------------> 2)OBJECT ASSIGNMENT
#include<stdio.h>
#include<string.h>

struct movie{
	
	char mName[20];
	int NoOfTick;
	float price;

};
void main(){
	
	//1st way (GIVING INITIALIZER LIST)
	struct movie obj1={"JAYHIND",3,40.67};//*************OBJECT INITIALIZATION***************

	printf("%s\n",obj1.mName);
	printf("%d\n",obj1.NoOfTick);
	printf("%f\n",obj1.price);
	
	//2nd way (ASSINING VALUE ONE BY ONE)
	struct movie obj3; //**************************OBJECT ASSIGNMENT**********************

	//obj3.mName="RRR"; **ERROR**
	
	//while assining string values always use strcpy 
	
	strcpy(obj3.mName,"RRR");
	obj3.NoOfTick=5;
	obj3.price=450.70;

	printf("%s\n",obj3.mName);
	printf("%d\n",obj3.NoOfTick);
	printf("%f\n",obj3.price);

}
