

//TYPEDEF IN STRUCTure :

#include<stdio.h>

struct movie {

	char mName[20];
	int count;
	float rating;

}obj1={"DRISHYAM",2,4.5};

void main(){

	typedef struct movie mv; //we can write mv instead of struct movie
	
	mv obj2={"RRR",3,6.5};

	struct movie *ptr1= &obj1;

	mv *ptr2=&obj2;

	printf("%s\n",(*ptr1).mName);
	printf("%d\n",(*ptr1).count);
	printf("%f\n",ptr1->rating);


	printf("%s\n",(*ptr2).mName);
	printf("%d\n",(*ptr2).count);
	printf("%f\n",ptr2->rating);

}
