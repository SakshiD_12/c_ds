
//NESTED STRUCTURE 
//*********************************1st WAY***************************************************
#include<stdio.h>
#include<string.h>

struct movieInfo{ //---------------->CREATING OBJECT OUTSIDE

	char actor[20];
	float imdb;

};
struct movie{

	char mName[20];
	struct movieInfo obj1; //--------------AND OBJECT INSIDE
};
void main(){

	struct movie obj2; //------------->MAIN STRUCTURE OBJECT

	strcpy(obj2.mName,"RRR");
	strcpy(obj2.obj1.actor,"Rishabh");
	
	obj2.obj1.imdb=9;

	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.imdb);

}
