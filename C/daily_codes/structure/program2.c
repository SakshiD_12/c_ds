
//STRUCTURE STORES DATA OF DIFFERENT DATA TYPES

//ASSINING VALUES TO STRUCTURE(1st way)
#include<stdio.h>

struct cricplayer{

	char pName[20]; //BASE OF 4 AS IT IS GREATER AMONG ALL DATA TYPES HENCE CHAR ARRAY OF 20 REQIRES 20 BOX OF 1
	int jerNo;      //FOR EACH CHAR HENCE IT IS 20 + 4(INT) + 4(FLOAT)
	float avg;
};
void main(){
	
	//assininh direct values to object
	struct cricplayer obj1={"VK",18,50.30};

	//printing the given value 
	printf("%s\n",obj1.pName);
	printf("%d\n",obj1.jerNo);
	printf("%f\n",obj1.avg);

	//size of structure
	printf("%ld\n",sizeof(obj1)); //28
	printf("%ld\n",sizeof(struct cricplayer)); //28

}

	
