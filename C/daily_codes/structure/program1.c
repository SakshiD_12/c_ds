
//STRUCTURE STORES DATA OF DIFFERENT DATA TYPES

//PROGRAM TO CALCULATE SIZE OF STRUCTURE
#include<stdio.h>

struct cricplayer{

	char pName[20]; //BASE OF 4 AS IT IS GREATER AMONG ALL DATA TYPES HENCE CHAR ARRAY OF 20 REQIRES 20 BOX OF 1
	int jerNo;      //FOR EACH CHAR HENCE IT IS 20 + 4(INT) + 4(FLOAT)
	float avg;
};
void main(){

	struct cricplayer obj;

	//same lines
	
	printf("%ld\n",sizeof(obj)); //28
	printf("%ld\n",sizeof(struct cricplayer)); //28

}

	
