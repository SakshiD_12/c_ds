
//ARRAY OF STRUCTURE (object)

#include<stdio.h>

struct player{

	char pName[20];
	int jerNo;
	float avg;

};
void main(){

	struct player obj1={"VK",18,4.6};
	struct player obj2={"MD",07,5.60};
	struct player obj3={"KL",06,3.6};

	//struct player arr[3]={&obj1,&obj2,&obj3}; **********WARNING GIVES WRONG VALUES************
	struct player arr[3]={obj1,obj2,obj3};

	//to print the values
	for(int i=0;i<3;i++){

		printf("%s\n",arr[i].pName);
		printf("%d\n",arr[i].jerNo);
		printf("%f\n",arr[i].avg);

	}
}
