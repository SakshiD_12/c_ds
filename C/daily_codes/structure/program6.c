

//INITIALIZING OBJECT(2WAYS)
//--------------------------> 1)OBJECT INITIALIZATION
//--------------------------> 2)OBJECT ASSIGNMENT

//WITHOUT USING LIBRARY FUNCTION
#include<stdio.h>

struct movie{
	
	char mName[20];
	int NoOfTick;
	float price;

};
char* mystrcpy(char *dest,char *src){
	while(*src !='\0'){
		*dest=*src;
		dest++;
		src++;
	}

	*dest='\0';
	return dest;
}

void main(){
	
	//1st way (GIVING INITIALIZER LIST)
	struct movie obj1={"JAYHIND",3,40.67};//*************OBJECT INITIALIZATION***************

	printf("%s\n",obj1.mName);
	printf("%d\n",obj1.NoOfTick);
	printf("%f\n",obj1.price);
	
	//2nd way (ASSINING VALUE ONE BY ONE)
	struct movie obj3; //**************************OBJECT ASSIGNMENT**********************

	//obj3.mName="RRR"; **ERROR**
	
	//while assining string values always use strcpy 
	
	mystrcpy(obj3.mName,"MAIN TERA HERO");
	obj3.NoOfTick=5;
	obj3.price=450.70;

	printf("%s\n",obj3.mName);
	printf("%d\n",obj3.NoOfTick);
	printf("%f\n",obj3.price);

}
