
//NESTED STRUCTURE 
//*********************************2nd WAY***************************************************
#include<stdio.h>
#include<string.h>

struct movieInfo{ //---------------->CREATING OBJECT OUTSIDE

	char actor[20];
	float imdb;

};
struct movie{

	char mName[20];
	struct movieInfo obj1; //--------------AND OBJECT INSIDE
};
void main(){

	struct movie obj2={"RRR",{"RISHAB",9.89}}; //---------->BY GIVING INITIALIZER LIST

	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.imdb);

}
