

//PASSING STRUCTURE TO FUNCTION 

#include<stdio.h>

struct Demo{

	int x;
	int y;

};
void fun(struct Demo *ptr){ //-------------->HENCE TAKING ADDRESS USING POINTER
	
	printf("%d\n",ptr->x);
	printf("%d\n",(*ptr).y);

}
void gun(struct Demo obj){ //***************HENCE TAKING OJECT 

	printf("%d\n",obj.x);
	printf("%d\n",obj.y);

}
void main(){

	struct Demo obj={10,20};

	fun(&obj); //---------->PASSING ADDRESS OF OBJECT
	gun(obj); //************PASSING OBJECT
}

