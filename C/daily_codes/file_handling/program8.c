

//ftell() : it tells the position of offset i.e file pointer 
//        :use %ld format specifier 


#include<stdio.h>
void main(){

	FILE *fp = fopen("info.txt","w");

	printf("%ld\n",ftell(fp)); //initially at 0th position  0

	fprintf(fp,"MUKTANGAN");

	printf("%ld\n",ftell(fp)); //9

	rewind(fp); //it will take fpback at the start of the file

	printf("%ld\n",ftell(fp)); // 0



}
