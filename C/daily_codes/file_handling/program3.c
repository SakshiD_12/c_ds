
//FILE RELATED FUNCTION 

//1)fopen("filename","mode") :TAKES TWO PARAMETERS 1ST IS FILE NAME AND 2ND IS IN WHICH MODE WE WANT TO OPEN THE FILE 
//--->"FOR READ MODE" : if file is present it will return the address of structure
//                    : if file is not present it will return nil
 

#include<stdio.h>

void main(){

	FILE *fp = fopen("Sakshi.txt","r");

	printf("%p\n",fp); // (nil)

}
