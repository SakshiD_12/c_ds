
//3)fscanf() : Read the data from file
//           :scanf() internally calls the fscanf()
//           :it takes 3 parameters ("from where to read","format specifier","address of box")


#include<stdio.h>
void main(){

	FILE *fp= fopen("Sakshi.txt","r");
	
	char ch;

	fscanf(fp,"%c",&ch);
	
	printf("%c",ch); //IT WILL READ ONLY FIRST CHARACTER FROM FILE

}
