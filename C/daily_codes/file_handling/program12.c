

//fputc()--->"WRITING CHARACTER IN FILE"
//           "CAN USE THIS FUNCRION TO COPY CONTENT OF ONE FILE TO ANOTHER"


#include<stdio.h>
void main(){

	FILE *fp1 = fopen("Sakshi.txt","r");

	FILE *fp2 = fopen("Copy1.txt","w");

	char ch;
	
	while((ch=fgetc(fp1)) != EOF){

		fputc(ch,fp2);

	}
}
