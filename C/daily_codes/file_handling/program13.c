

//fgets()--->"READING STRING FROM FILE"
//       --->TAKES 3 PARAMETERES : 1.where to store reading character
//                                 2.how much character you want to read
//                                 3. from where to read 

#include<stdio.h>
void main(){

	FILE *fp = fopen("Sakshi.txt","r");

	char schlName[20]; //we require array to store string which are reading 

	fgets(schlName,10,fp);

	puts(schlName);

}
