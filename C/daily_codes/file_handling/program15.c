

//8)fclose() :used to close the file 
//           :take a file pointer as a parameter


#include<stdio.h>
void main(){

	FILE *fp = fopen("Sakshi.txt","r");

	printf("%c\n",fgetc(fp)); //READS THE FIRST CHARACTER ONLY FROM YHE FILE
	printf("%c\n",fgetc(fp)); //READS SEDCOND CHARACTER FROM THE FILE

	fclose(fp); 

	printf("%c\n",fgetc(fp)); //GARBAGE VALUE AS FILE IS CLOSED BEFORE THIS 

}

