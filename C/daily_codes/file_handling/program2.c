
//FILE RELATED FUNCTION 

//1)fopen("filename","mode") :TAKES TWO PARAMETERS 1ST IS FILE NAME AND 2ND IS IN WHICH MODE WE WANT TO OPEN THE FILE 

//--->"FOR WRITE MODE" : if file is present return the address of structure                      
//                     : if not present then this mode will create the file and then returns the address

#include<stdio.h>

void main(){

	FILE *fp = fopen("Demo.txt","w");

	printf("%p\n",fp); //address of structure

}
