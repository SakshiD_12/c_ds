
//9)fseek() : IT READS FROM THE RANDOM LOCATION 
//          : 0 :SEEK_SET "READS FROM STARTING"
//          : 1 :SEEK_CUR "READS FROM THE CURRENT POSITION OF POINTER"
//          : 2 :SEEK_END "READS FROM THE END OF THE FILE" (here 2nd parameter is always negative)

//IT HAS 3 PARAMETERS :(file pointer,read upto,from where to read)

#include<stdio.h>
void main(){

	FILE *fp = fopen("ABC.txt","r");

	//fseek(fp,-6,2);
	//fseek(fp,6,1);
	fseek(fp,7,0); //reads from the 7th character from starting

	char ch;

	while((ch =fgetc(fp)) != EOF){

		printf("%c",ch);

	}
}
