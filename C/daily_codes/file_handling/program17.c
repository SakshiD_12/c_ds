

//10)putw() :WRITES THE INTEGER VALUES IN THE FILE 
//11)getw() :READS THE INTEGER DATA FROM THE FILE (READS THE DATA OF 4 4 BYTE)

#include<stdio.h>
void main(){

	FILE *fp = fopen("XYZ.txt","w+");

	int num1=10;
	int num2=20;

	printf("%ld\n",ftell(fp)); //0

	putw(num1,fp);
	putw(num2,fp); 

	printf("%ld\n",ftell(fp)); 

	rewind(fp);

	printf("%d\n",getw(fp));
	printf("%d\n",getw(fp));

}


