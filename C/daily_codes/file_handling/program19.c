

//WAP TO READ THE FIRST 10 CHARACTER FROM THE FILE (USING BREAK)

#include<stdio.h>
void main(){

	FILE *fp = fopen("ABC.txt","r");

	char ch;

	int n=10;
	
	int x=0;

	while((ch=fgetc(fp)) != EOF && n != 0){
		
		if(x == n){

			break;

		}

		printf("%c",ch);
		x++;
	}
}
