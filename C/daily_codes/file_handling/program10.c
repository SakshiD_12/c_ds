

//fgetc()--->"READS THE CHARACTER FROM FILE"

//COUNT THE NUMBER OF CHARACTER IN THE FILE USE fgetc()
#include<stdio.h>
void main(){

	FILE *fp = fopen("Demo.txt","r");

	int count=0;
	
	while((fgetc(fp) != EOF)){

		count++;

	}
	printf("%d\n",count);
}
