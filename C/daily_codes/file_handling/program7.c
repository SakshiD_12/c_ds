
//APPEND MODE : it will write at the end of the file 
//            : if we execute the file agian again then it will add the content at the end of file everytime
//            : in append mode if file is not present it will create it

#include<stdio.h>

void main(){

	FILE *fp = fopen("class.txt","a");

	fprintf(fp,"GOOD MORNING");

}
