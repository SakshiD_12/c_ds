
//ENUM IS JUST ONLY USED TO MAKE CODE READABLE BY ASSINING INTEGER CONSTANT VALUES IT IS BY DEFAULT VALUE STARTS FROM 0

#include<stdio.h>

 enum partners{

	 sakshi, //0
	 sayali, //1
	 mansi, //2 
	 vaibhu, //3
	 pooja //4
 };
void main(){

	enum partners obj; //THERE IS NO NEED TO CREATE OBJECT IN ENUM WE CANNOT ACCESS ELEMENT IN ENUM USING OBJECT

/*	**********EROOR**********
	printf("%d\n",obj.sakshi);
	printf("%d\n",obj.vaibhu);
*/
	printf("%d\n",sakshi); //0
	printf("%d\n",mansi); //2
	printf("%d\n",pooja); //4

}
