
//CALL BY ADDRESS 
//SWAPPING X VALUE BY ACESSING ITS ADDRESS
#include<stdio.h>
void swap(int*);

void main(){

	int x;
	printf("ENTER VALUE OF X:\n");
	scanf("%d",&x);

	printf("IN MAIN\n");
	printf("x=%d\n",x);

	swap(&x);

	printf("IN MAIN AGAIN AFTER SWAPPING\n");
	printf("x=%d\n",x);
}
void swap(int *ptr){

	printf("IN FUNCTION BEFORE SWAPPING :%d\n",*ptr);

	int y;
	printf("ENTER VALUE TO SWAP :\n");
       	scanf("%d",&y);
	
	*ptr=y;	
	printf("IN FUNCTION AFTER SWAPPING :%d\n",*ptr);
}



