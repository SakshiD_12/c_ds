//FUNCTION POINTER
#include<stdio.h>

void add(int a,int b){

	printf("THE ADDITION IS = %d\n",a+b);
}
void sub(int a,int b){

	printf("THE SUBTRACTION IS = %d\n",a-b);
}
void main(){

	int x,y;
	printf("ENTER X AND Y VALUES :\n");
	scanf("%d%d",&x,&y);

	void (*ptr)(int,int);

	ptr=add;
	ptr(x,y);
	
	ptr=sub;
	ptr(x,y);
	
}
