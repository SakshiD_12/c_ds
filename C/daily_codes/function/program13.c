//calculating sum of array by passing whole array

#include<stdio.h>
int sumarray(int *ptr,int arrsize){
	
	int sum=0;

	for(int i=0;i<arrsize;i++){
		sum = sum+*(ptr+i);
	}
	return sum;
}
void main(){

	int arr[]={10,20,30,40,50};

	int arrsize=sizeof(arr)/sizeof(int);

	int sum = sumarray(arr,arrsize);
	
	printf(" SUM OF ARRAY : %d\n",sum);
	
}
