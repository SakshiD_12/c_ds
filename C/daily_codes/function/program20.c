//NEVER RETURN THE ADDRESS OF LOCAL VARIABLE
#include<stdio.h>

int val; //global variable

int *fun(int x,int y){

	printf("%d\n",x+y); //50
	val=x+y;

	printf("%p\n",&val); //address of val

	return &val; //warning

}
void main(){

	int *ptr=fun(20,30);
	printf("%p\n",ptr); //address of ptr
	printf("%d\n",*ptr); //50

}
