
//CALL BY VALUE
//SWAPPING OF TWO NUMBERS
#include<stdio.h>
void swap(int,int);

void main(){

	int x,y;
	printf("ENTER VALUE :\n");
	scanf("%d%d",&x,&y);

	printf("IN MAIN\n");
	printf("x=%d\n",x);
	printf("y=%d\n",y);

	swap(x,y);

	printf("IN MAIN AGAIN AFTER SWAPPING\n");
	printf("x=%d\n",x); //AFTER SWAPPING THE SCOPE OF SWAPPED VARIABLE IS FINISHED AFTER SWAP STACK FRAME IS POPED
	printf("y=%d\n",y); //HENCE HERE THERE IS NO EFFECT ON VALUE OF X AND Y SOLUTION IS TO CALLING BY ADDRESS
}
void swap(int x,int y){

	printf("IN FUNCTION BEFORE SWAPPING\n");
	printf("x=%d\n",x);
	printf("y=%d\n",y);

	int temp;
	
	temp=x;
	x=y;
	y=temp;

	printf("IN FUNCTION AFTER SWAPPING\n");
	printf("x=%d\n",x);
	printf("y=%d\n",y);
}
	




