//FUNCTION RETURNING MULTIPLE VALUES
#include<stdio.h>
void fun(int x,int y,int z,int *X,int *Y,int *Z){
	
	printf("IN FUNCTION :\n");
	*X=x;
	*Y=y;
	*Z=z;
}
void main(){

	int x,y,z;
	printf("ENTER X,Y AND Z VALUES :\n");
	scanf("%d%d%d",&x,&y,&z);

	int storeX,storeY,storeZ;
	fun(x,y,z,&storeX,&storeY,&storeZ);

	printf("X = %d\n",storeX);
	printf("Y = %d\n",storeY);
	printf("Z = %d\n",storeZ);
}



