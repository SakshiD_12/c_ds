//PASSING FUNCTION AS PARAMETER
#include<stdio.h>

void add(int x,int y){

	printf("%d\n",x+y);

}
void fun(int x,int y,void (*ptr)(int,int)){

	ptr(x,y);

}
void main(){

	fun(20,30,add);

}


