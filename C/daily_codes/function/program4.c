
#include<stdio.h>
void fun(int (*ptr)[]){

	printf("%p\n",ptr); //address of whole array
 	printf("%d\n",**ptr); //1
 	printf("%d\n",*(*ptr+2));//3
}
void main(){

	int arr[]={1,2,3,4,5};

	fun(&arr); //whole array address
}
