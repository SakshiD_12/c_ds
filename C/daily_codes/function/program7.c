
//CALL BY ADDRESS
//SWAPPING OF TWO NUMBERS
#include<stdio.h>
void swap(int*,int*);

void main(){

	int x,y;
	printf("ENTER VALUE :\n");
	scanf("%d%d",&x,&y);

	printf("IN MAIN\n");
	printf("x=%d\n",x);
	printf("y=%d\n",y);

	swap(&x,&y);

	printf("IN MAIN AGAIN AFTER SWAPPING\n");
	printf("x=%d\n",x); 
	printf("y=%d\n",y); 
}
void swap(int *ptr1,int *ptr2){

	printf("IN FUNCTION BEFORE SWAPPING\n");
	printf("x=%d\n",*ptr1);
	printf("y=%d\n",*ptr2);

	int temp;
	
	temp=*ptr1;
	*ptr1=*ptr2;
	*ptr2=temp;

	printf("IN FUNCTION AFTER SWAPPING\n");
	printf("x=%d\n",*ptr1);
	printf("y=%d\n",*ptr2);
}
	




