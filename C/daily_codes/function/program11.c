
//PASSING ARRAY TO A FUNCTION (PASSING WHOLE ARRAY)
#include<stdio.h>
void printElement(int *ptr,int arrsize){

	for(int i=0;i<arrsize;i++){

	printf("%d\n",*(ptr+i));  // or ptr[]

	}
}
void main(){

	int arr[]={10,20,30,40,50};

	int arrsize=sizeof(arr)/sizeof(int);
	
	printElement(arr,arrsize);
}
