
//PASSING ARRAY TO A FUNCTION (USING ARRAY)
#include<stdio.h>
void printElement(int arr[],int arrsize){
	
	for(int i=0;i<arrsize;i++){

	printf("%d\n",*(arr+i)); //or arr[i]
	
	}

}
void main(){

	int arr[]={10,20,30,40,50};

	int arrsize=sizeof(arr)/sizeof(int);
	
	printElement(arr,arrsize);

}
