#include<stdio.h>
void main(){
	printf("START MAIN\n");
	int x=1;

	if(--x){
		printf("YOU ARE IN FIRST IF\n"); //0 so this block will not execute
	}
	if(++x || x++){
		printf("YOU ARE IN SECOND IF\n"); 
	}
	printf("x=%d\n",x);//1

	printf("END MAIN\n");
}
