#include<stdio.h>
void main(){
	printf("START MAIN\n");
	int x=10;
	char ch1='A';
	char ch2=97;
	char ch3=128;
	char ch4=129;
	char ch5=130;

	printf("%d\n",x); //10

	printf("%c\n",ch1); //A
	printf("%d\n",ch1); //65

	printf("%c\n",ch2); //a
	printf("%d\n",ch2);//97

	printf("%c\n",ch3); //not defined
	printf("%d\n",ch3); //-128

	printf("%c\n",ch4); //not defined
	printf("%d\n",ch4); //-127

	printf("%c\n",ch5); //not defined
	printf("%d\n",ch5); //-126

	printf("END MAIN\n");
}
