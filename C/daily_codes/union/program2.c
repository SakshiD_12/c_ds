
//UNION--->In union only one element can get memory at one time and that elemt size is greater amongs all

//CALCULATING SIZE OF UNION 

#include<stdio.h>
#include<string.h>

union book{
	
	char bName[20]; //here it will consider 8 as a base but char array has large element i.e 20 hence it require 24 block to
	char author[20];//store 20 char
	int pages;
	double price;
};
void main(){

//	union book bk={"os","ag",310,450.90}; //warning for all element except os cuz union gives value one at time to resolve 
                                             //this problem assign one by one value 

	union book bk;

	strcpy(bk.bName,"OS");
	printf("%s\n",bk.bName);

	strcpy(bk.author,"AGODBOLE");
	printf("%s\n",bk.author);

	bk.pages=310;
	printf("%d\n",bk.pages);

	bk.price=450.78;
	printf("%f\n",bk.price);

	printf("%ld\n",sizeof(union book)); //24

}

