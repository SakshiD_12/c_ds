//COPY THE STRING WITHOUT USING LIBRARY FUNCTION

#include<stdio.h>

char* mystrcpy(char *dest,char *src){
	
	while(*src != '\0'){

		*dest=*src;
		src++;
		dest++;
	}

	*dest='\0';
	return dest;

}
void main(){

	char *str1="SAKSHI DHUMAL";

	char str2[20];

	mystrcpy(str2,str1);

	puts(str1);
	puts(str2);

}
