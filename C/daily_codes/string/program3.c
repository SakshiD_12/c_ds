//CHANGING ELEMENTS IN STRING
#include<stdio.h>
void main(){

	char carr[]={'S','A','K','S','H','I','\0'}; // \0 reprent the end of string

	char *str="SAKSHI"; //by default \0 is consider here

	printf("%s\n",carr); //SAKSHI
	printf("%s\n",carr); //SAKSHI

	carr[2]='Y'; //CHANGING 2nd INDEX OF CHARACTER ARRAY BY X

	printf("%s\n",carr); //SAYSHI

	*str='B'; //IT IS READ ONLY DATA SO WE CANT CHANGE IT

	printf("%s\n",str); //SEGMETATION FAULT

}
