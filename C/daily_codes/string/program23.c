//mystrncpy() :COPYING ONLY N CHARACTERS FROM STRING
#include<stdio.h>

char* mystrncpy(char* str2,char* str1,int n){
	
	while(n != 0 && *str1 !='\0'){

		*str2=*str1;
		str1++;
		str2++;
		n--;
	}

	*str2='\0';

	return  str2;
}
void main(){

	char *str1="RAHUL PICHE";
	char str2[20];
	
	int n=4;
	mystrncpy(str2,str1,n);

	puts(str2);
}

