//CALCULATING LENGTH OF STRING WITHOUT USING LIBRARY FUNCTION

#include<stdio.h>

int mystrlen(char* str){
	
	int count=0;
	while(*str != '\0'){

		count++;
		str++;
	}
	return count;
}
void main(){

	char str1[]={'S','A','K','S','H','I','\0'}; //6

	char *str2="SAKSHI DHUMAL"; //13

	printf("%d\n",mystrlen(str1));
	printf("%d\n",mystrlen(str2));

}
