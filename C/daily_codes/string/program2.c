//CHANGING ELEMENTS IN STRING
#include<stdio.h>
void main(){

	char carr[]={'S','A','K','S','H','I','\0'}; // \0 reprent the end of string

	char *str="SAKSHI"; //by default \0 is consider here

	printf("%s\n",carr); //SAKSHI
	printf("%s\n",carr); //SAKSHI

	printf("\n");

	carr[4]='X';

	printf("%s\n",carr); //SAKSXI
	printf("%s\n",str); //SAKSHI

	printf("\n");

	printf("%c\n",*str); //str points to first element is string i.e S
	printf("%c\n",*(carr+3)); //carr means address of first element + 3 i.e S


}
