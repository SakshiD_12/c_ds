//ARRAY OF STRINGS

#include<stdio.h>
void main(){
	
	//TWO WAYS TO INITIALIZE ARRAY OF STRING 
	char arr1[3][10]={"ASHISH","KANHA","RAHUL"};
	char arr2[][20]={
		         {'A','N','I','L','\0'},
		         {'A','M','I','T','\0'},
		         {'A','J','I','T','\0'} 
	};

	printf("%s\n",arr1[0]); //ASHISH
	puts(arr2[0]); //ANIL

}
