//COMPARING TWO STRINGS WITHOUT USING LIBRARY FUNCTION :

#include<stdio.h>
#include<string.h>

int mystrcmp(char* str1,char* str2){
	int count=0;
	while(*str1 != '\0'){
		if(*str1 == *str2){ 

			str1++;
			str2++;

		}else{

			return *str1 - *str2;

		}
	}

	return 0;
}

void main(){

	char *str1="Sakshi";

	char *str2="SaksHi";

	int diff=1;

	if(strlen(str1)==strlen(str2)){

	     diff=mystrcmp(str1,str2);

	}

	printf("%d\n",diff);

	if(diff==0){

		printf("EQUAL\n");

	}else{
		printf("NOT EQUAL\n");

	}
}
