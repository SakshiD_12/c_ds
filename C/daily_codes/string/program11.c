// CALCULATING LENGTH OF STRING USING LIBRARY FUNCTION strlen() :

#include<stdio.h>
#include<string.h>
void main(){

	char *str1="SAKSHI DHUMAL";

	char str2[]={'S','A','K','S','H','I','\0'};

	int lenstr1 = strlen(str1);
	int lenstr2 = strlen(str2);

	printf("%d\n",lenstr1); //13
	printf("%d\n",lenstr2); //6

}
