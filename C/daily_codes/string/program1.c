//INITIALIZING ARRAY
//TWO WAYS--------------1.USING CHARACTER ARRAY 2.USING CHARACTER POINTER
#include<stdio.h>
void main(){

	char carr[]={'S','A','K','S','H','I','\0'}; // \0 reprent the end of string

	char *str="SAKSHI"; //by default \0 is consider here

	printf("%s\n",carr);
	printf("%s\n",str);

}
