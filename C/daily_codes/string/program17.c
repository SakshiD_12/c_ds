//COMPARING TWO STRINGS USING LIBRARY FUNCTION strcmp() :

#include<stdio.h>
#include<string.h>

void main(){

	char *str1="Sakshi";
	char *str2="Sakshi";

	int diff=strcmp(str1,str2);
	printf("%d\n",diff);

	if(diff==0){
		printf("EQUAL\n");
	}else{
		printf("NOT EQUAL\n");
	}
}
