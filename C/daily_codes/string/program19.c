//REVERSING STRING (THERE IS NO FUNCTION LIKE strrev() IN C) :

#include<stdio.h>

char* mystrrev(char* str){
	
	char* temp=str; 

	while(*temp !='\0'){

		temp++;

	}

	temp--;

	char x;

	while(str<temp){

		x=*str;
		*str=*temp;
		*temp=x;

		str++;
		temp--;

	}

	return str;
}
void main(){
	
	char str[20];
	printf("ENTER STRING :\n");
	scanf("%s",str);
	
	puts(str); 

	mystrrev(str);
	
	printf("AFTER REVERSING :\n");
	puts(str);

}

