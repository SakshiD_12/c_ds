//CALCULATING SIZE OF STRING USING sizeof()
#include<stdio.h>
void main(){

	char carr[]={'S','A','K','S','H','I','\0','D','H','U','M','A','L'};

	char *str="SAKSHI";

	printf("%ld\n",sizeof(carr)); //including \0 returns the size i.e 13
	printf("%ld\n",sizeof(str)); //as it is pointer its size is 8 byte

	printf("PRINTING USING PUTS() :\n");

	puts(carr);//here it will read only upto \0 i.e SAKSHI
	puts(str); //SAKSHI

	printf("PRINTING USING FOR LOOP() :\n");

	//printing character array using for loop here it force to read upto i<=13
	for(int i=0;i<=13;i++){
		printf("%c",carr[i]);
	}
	printf("\n");
	//printing character string using for loop
	for(int i=0;i<7;i++){
		printf("%c",str[i]);
	}
	printf("\n");

}
