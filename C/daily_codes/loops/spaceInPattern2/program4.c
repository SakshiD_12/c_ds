//1 2 3 4
//  5 6 7
//    8 9
//     10
#include<stdio.h>
void main(){
	int row;
	printf("ENTER NUMBER OF ROWS :\n");
	scanf("%d",&row);
	int x=1;
	for(int i=1;i<=row;i++){
		for(int sp=0;sp<=i;sp++){
			printf("  ");
		}
		for(int j=row;j>=i;j--){
			printf("%d ",x);
			x++;
		}
		printf("\n");
	}
}
