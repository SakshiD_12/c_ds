//  2 4 6 8
//    10 12 14
//       16 18
//          20
#include<stdio.h>
void main(){
	int row;
	printf("ENTER NUMBER OF ROWS :\n");
	scanf("%d",&row);
	int x=1;
	for(int i=1;i<=row;i++){
		for(int sp=0;sp<=i;sp++){
			printf("  ");
		}
		for(int j=row;j>=i;j--){
			printf("%d ",x*2);
			x++;
		}
		printf("\n");
	}
}
