


//CONTINUE IS ONLY USED ON THE LOOPS 
//CONTINUE STATEMENT IS USED TO SKIP THE ITERATION IN THE LOOP


#include<stdio.h>
void main(){

	for(int i=1;i<10;i++){

		if(i==7){
			continue; //it will skip the 7 
		}else{
			printf("%d\n",i);
		}
	}
}
