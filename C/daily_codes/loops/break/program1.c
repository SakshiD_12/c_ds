

//BREAK IS ONLY USED ON LOOPS
//THE BREAK STATEMENT IS USED TO EXIT FROM THE LOOP

#include<stdio.h>
void main(){

	for(int i=1;i<10;i++){

		if(i==5){
			break;
		}else{
			printf("%d\n",i);
		}
	}
}
