//      A
//    B B
//  C C C
//D D D D          
#include<stdio.h>
void main(){
	int row;
	printf("ENTER NUMBER OF ROWS :\n");
	scanf("%d",&row);
	char ch='A';
	for(int i=1;i<=row;i++){
		for(int sp=row;sp>=i;sp--){
			printf("  ");
		}
		for(int j=1;j<=i;j++){
			printf("%c ",ch);
		}
		printf("\n");
		ch++;
	}
}
