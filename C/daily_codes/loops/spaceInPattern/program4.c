//      a
//    b c
//  d e f
//g h i j         
#include<stdio.h>
void main(){
	int row;
	printf("ENTER NUMBER OF ROWS :\n");
	scanf("%d",&row);
	char ch='a';
	for(int i=1;i<=row;i++){
		for(int sp=row;sp>=i;sp--){
			printf("  ");
		}
		for(int j=1;j<=i;j++){
			printf("%c ",ch);
			ch++;
		}
		printf("\n");
	}
}
