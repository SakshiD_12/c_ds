
//WAP TO PRINT THE EVEN NUMBERS IN REVERSE ORDER AND ODD NUMBERS IN STANDARD WAY

#include<stdio.h>
void main(){
	int rs,re;
	printf("ENTER RANGE :\n");
	scanf("%d%d",&rs,&re);
	printf("EVEN NUMBERS :");
	for(int i=re;i>=rs;i--){
		if(i%2==0){
			printf("%d  ",i);
		}
	}

	
	printf("\n");
	printf("ODD NUMBERS :");
	for(int i=rs;i<=re;i++){
		if(i%2==1){
			printf("%d ",i);
		}
	}
}
